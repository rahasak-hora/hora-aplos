package com.score.aplos.actor

import akka.actor.{Actor, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import akka.util.Timeout
import com.score.aplos.actor.HttpActor.{Serve, Status}
import com.score.aplos.actor.WatchActor.{Create, Get, Put, Transfer}
import com.score.aplos.config.AppConf
import com.score.aplos.protocol.WatchMessage
import com.score.aplos.util.AppLogger

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object HttpActor {

  case class Status(code: Int, msg: String)

  case class Serve()

  def props()(implicit system: ActorSystem) = Props(new HttpActor)

}

class HttpActor()(implicit system: ActorSystem) extends Actor with AppLogger with AppConf {

  override def receive: Receive = {
    case Serve =>
      // supervision
      // meterializer for streams
      val decider: Supervision.Decider = { e =>
        logError(e)
        Supervision.Resume
      }
      implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
      implicit val ec = system.dispatcher

      Http().bindAndHandle(route, "0.0.0.0", 8761)
  }

  def route()(implicit ec: ExecutionContextExecutor) = {
    implicit val timeout = Timeout(10.seconds)
    import com.score.aplos.protocol.WatchMessageProtocol._
    implicit val format = jsonFormat2(Status.apply)

    pathPrefix("api") {
      path("aplos") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "aplos contract"))
        } ~
          post {
            entity(as[WatchMessage]) {
              case create: Create =>
                println("Create")

                val f = context.actorOf(WatchActor.props()) ? create
                onComplete(f) {
                  case Success(status: Int) =>
                    complete(Status(status, "done"))
                  case Failure(e) =>
                    e.printStackTrace()
                    complete(Status(400, "error"))
                  case _ =>
                    complete(Status(400, "error"))
                }
              case put: Put =>
                context.actorOf(WatchActor.props()) ! put
                complete(StatusCodes.OK)
              case get: Get =>
                context.actorOf(WatchActor.props()) ! get
                complete(StatusCodes.OK)
              case transfer: Transfer =>
                context.actorOf(WatchActor.props()) ! transfer
                complete(StatusCodes.OK)
            }
          }
      }
    }
  }

}
