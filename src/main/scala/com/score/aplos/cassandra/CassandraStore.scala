package com.score.aplos.cassandra

import java.util.Date

import com.score.aplos.config.SchemaConf
import com.score.aplos.util.AppLogger

case class Watch(id: String, model: String, color: String, owner: String, timestamp: Date = new Date)

case class Trans(id: String, execer: String, actor: String, message: String,
                 watchId: String, watchModel: String, watchColor: String, watchOwner: String,
                 timestamp: Date = new Date)

object CassandraStore extends CassandraCluster with SchemaConf with AppLogger {

  lazy val dsps = session.prepare("SELECT id FROM mystiko.trans where execer = ? AND id = ? LIMIT 1")
  lazy val cwps = session.prepare("INSERT INTO mystiko.watches(id, model, color, owner, timestamp) VALUES(?, ?, ?, ?, ?)")
  lazy val gwps = session.prepare("SELECT * FROM mystiko.watches where id = ? LIMIT 1")
  lazy val uwps = session.prepare("UPDATE mystiko.watches SET owner = ? WHERE id = ?")
  lazy val ctps = session.prepare("INSERT INTO mystiko.trans(id, execer, actor, message, watch_id, watch_model, watch_color, watch_owner, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)")

  def init() = {
    // create keyspace
    // create udt type
    // create table
    session.execute(schemaCreateKeyspace)
    session.execute(schemaCreateTypeTrans)
    session.execute(schemaCreateTableTrans)
    session.execute(schemaCreateTableWatches)
  }

  def isDoubleSpend(execer: String, id: String): Boolean = {
    // check weather given trans with id exists
    val row = session.execute(dsps.bind(execer, id)).one()
    row != null
  }

  def createWatch(watch: Watch) = {
    // create watch
    session.execute(cwps.bind(watch.id, watch.model, watch.color, watch.owner, watch.timestamp))
  }

  def getWatch(id: String): Option[Watch] = {
    // watch object
    val row = session.execute(gwps.bind(id)).one()
    if (row != null) Option(Watch(row.getString("id"), row.getString("model"),
      row.getString("color"), row.getString("owner"), row.getTimestamp("timestamp")))
    else None
  }

  def updateOwner(id: String, owner: String) = {
    // update owner
    session.execute(uwps.bind(owner, id))
  }

  def createTrans(trans: Trans) = {
    // create trans
    session.execute(ctps.bind(trans.id, trans.execer, trans.actor, trans.message,
      trans.watchId, trans.watchModel, trans.watchColor, trans.watchOwner, trans.timestamp))
  }
}

//object M extends App {
//  CassandraStore.init()
//  println(CassandraStore.isDoubleSpend("eranga", "112"))
//  CassandraStore.createWatch(Watch("1111", "tisot", "black", "eranga"))
//  println(CassandraStore.getWatch("1111"))
//  //CassandraStore.updateOwner("1111", 7880)
//  CassandraStore.createTrans(Trans("112", "eranga", "WatchActor", "{\"message\": \"java\"}", "1111", "tisot", "black", "eranga"))
//}