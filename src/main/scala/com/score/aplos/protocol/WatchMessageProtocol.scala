package com.score.aplos.protocol

import com.score.aplos.actor.WatchActor._
import spray.json._

trait WatchMessage

object WatchMessageProtocol extends DefaultJsonProtocol {

  implicit val initFormat: JsonFormat[Create] = jsonFormat7(Create)
  implicit val putFormat: JsonFormat[Put] = jsonFormat7(Put)
  implicit val getFormat: JsonFormat[Get] = jsonFormat4(Get)
  implicit val transferFormat: JsonFormat[Transfer] = jsonFormat7(Transfer)

  implicit object WatchMessageFormat extends RootJsonFormat[WatchMessage] {
    def write(obj: WatchMessage): JsValue =
      JsObject((obj match {
        case i: Create => i.toJson
        case p: Put => p.toJson
        case g: Get => g.toJson
        case t: Transfer => t.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): WatchMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("create")) => json.convertTo[Create]
        case Seq(JsString("put")) => json.convertTo[Put]
        case Seq(JsString("get")) => json.convertTo[Get]
        case Seq(JsString("transfer")) => json.convertTo[Transfer]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}

//object M extends App {
//
//  import WatchMessageProtocol._
//
//  val i = Create("create", "eraga", "23121", "111", "tisslot", "black", "eranga")
//  val s = i.toJson.toString
//  println(s)
//  s.parseJson.convertTo[WatchMessage] match {
//    case i: Create => println(s"init $i")
//    case p: Put => println(s"put $p")
//    case g: Get => println(s"get $g")
//  }
//
//}

