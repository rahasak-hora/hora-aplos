package com.score.aplos.actor

import akka.actor.{Actor, Props}
import com.score.aplos.actor.WatchActor._
import com.score.aplos.cassandra.{CassandraStore, Trans, Watch}
import com.score.aplos.protocol.WatchMessage
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.AppLogger
import spray.json.DefaultJsonProtocol._
import spray.json._

object WatchActor {

  case class Create(messageType: String, execer: String, id: String, watchId: String, watchModel: String, watchColor: String, watchOwner: String) extends WatchMessage

  case class Put(messageType: String, execer: String, id: String, watchId: String, watchModel: String, watchColor: String, watchOwner: String) extends WatchMessage

  case class Get(messageType: String, execer: String, id: String, watchId: String) extends WatchMessage

  case class Transfer(messageType: String, execer: String, id: String, watchId: String, watchModel: String, watchColor: String, watchOwner: String) extends WatchMessage

  def props() = Props(new WatchActor)

}

class WatchActor extends Actor with AppLogger {
  override def receive: Receive = {
    case create: Create =>
      logger.info(s"got create message - $create")

      // first check double spend
      val id = s"${create.execer};${create.id}"
      if (!CassandraStore.isDoubleSpend(create.execer, create.id) && RedisStore.set(id)) {
        // create trans
        implicit val format: JsonFormat[Create] = jsonFormat7(Create)
        val trans = Trans(create.id, create.execer, "WatchActor", create.toJson.toString, create.watchId, create.watchModel, create.watchColor, create.watchOwner)
        CassandraStore.createTrans(trans)

        // create watch on mystiko cassandra
        val watch = Watch(create.watchId, create.watchModel, create.watchColor, create.watchOwner)
        CassandraStore.createWatch(watch)

        logger.info(s"init done $create")
        sender ! 201
      } else {
        logger.error(s"double spend init - $create")
        sender ! 400
      }
    case put: Put =>
      logger.info(s"got put message - $put")

      // first check double spend
      val id = s"${put.execer};${put.id}"
      if (!CassandraStore.isDoubleSpend(put.execer, put.id) && RedisStore.set(id)) {
        // create trans
        implicit val format: JsonFormat[Put] = jsonFormat7(Put)
        val trans = Trans(put.id, put.execer, "WatchActor", put.toJson.toString, put.watchId, put.watchModel, put.watchColor, put.watchOwner)
        CassandraStore.createTrans(trans)

        // update watch on mystiko cassandra

        logger.info(s"put done - $put")
        sender ! 200
      } else {
        logger.error(s"double spend put $put")
        sender ! 400
      }
    case get: Get =>
      logger.info(s"got get message - $get")

      // get watch from mystiko cassandra
      // no need to create transaction here since not updating the ledger
      sender ! CassandraStore.getWatch(get.watchId)
    case transfer: Transfer =>
      logger.info(s"got transfer message - $transfer")

      // first check double spend
      val id = s"${transfer.execer};${transfer.id}"
      if (!CassandraStore.isDoubleSpend(transfer.execer, transfer.id) && RedisStore.set(id)) {
        // create trans
        implicit val format: JsonFormat[Transfer] = jsonFormat7(Transfer)
        val trans = Trans(transfer.id, transfer.execer, "WatchActor", transfer.toJson.toString, transfer.watchId, "", "", transfer.watchOwner)
        CassandraStore.createTrans(trans)

        // update watch owner
        val watch = CassandraStore.getWatch(transfer.watchId)
        watch match {
          case Some(f) =>
            CassandraStore.updateOwner(f.id, transfer.watchOwner)
            logger.info(s"transfer done - $transfer")
            sender ! 200
          case _ =>
            logger.error(s"no matching watch found for transfer - $transfer")
            sender ! 400
        }
      } else {
        // error since double spend
        logger.error(s"double spend transfer $transfer")
        sender ! 400
      }
  }
}
